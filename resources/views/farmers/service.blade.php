@extends('layouts.template')

@section('content')
 


  <div class=" box box-success ">
  <br>
  <div class="login-logo">
    <a href=""><b>Avepo</b>Vet Services</a>
  </div>

   
<form method="POST" action="{{ url('payservice') }}">
@csrf
 <div class="box-body " >

<div class="form-group has-feedback"> 
<label for="role" class=" col-form-label text-md-right">{{ __('Service being offered') }}</label>
<select id="id" class="form-control" name="service" value="{{ old('role') }}" required autofocus>
  <option >Artificial Insemination (A.I)</option>
  <option >Artificial Insemination (Repeat)</option>
  <option >Veterinary clinical services </option>
</select>

                        
</div>

<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Date') }}</label>
 <input id="date" type="date"  class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="date" value="{{ old('date') }}" required autofocus>
    
                        
</div>

<div class="form-group has-feedback">
<label for="id_no" class=" col-form-label text-md-right">{{ __('Name') }}</label>
 <select name="customer" class="form-control">
    @foreach($customer as $user)
<option value="{{$user->id}}">{{$user->name}}</option>
  @endforeach 
 </select>                        
</div>

<div class="form-group has-feedback">
<label for="id_no" class=" col-form-label text-md-right">{{ __('Vet Name ') }}</label>
 <select name="vet" class="form-control">
  @foreach($vet as $user)
<option value="{{$user->id}}">{{$user->name}}</option>
  @endforeach 
 </select>                        
</div>

<div class="form-group has-feedback">
<label for="cost" class=" col-form-label text-md-right">{{ __('Cost Of Service') }}</label>
 <input id="cost" type="text" placeholder="Cost of Service Offered" class="form-control{{ $errors->has('cost') ? ' is-invalid' : '' }}" name="cost" value="{{ old('cost') }}" required autofocus>
       @if ($errors->has('cost'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('cost') }}</strong>
      </span>
       @endif
                        
</div>

<div class="form-group has-feedback">
<label for="id_no" class=" col-form-label text-md-right">{{ __('Description of Service ') }}</label>
 <textarea  name="desc" row="4" class="form-control"></textarea>                   
</div> 

<div class="form-group has-feedback">
<label for="paid" class=" col-form-label text-md-right">{{ __('Amount Paid*') }}</label>
 <input id="paid" type="text" placeholder="0.00" class="form-control{{ $errors->has('paid') ? ' is-invalid' : '' }}" name="paid" value="{{ old('paid') }}" required autofocus>
       @if ($errors->has('paid'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('paid') }}</strong>
      </span>
       @endif
                      
</div> </div>
  <div class="box-footer">
                <button type="submit" class="form-control btn btn-primary">Submit</button>
              </div>
    </form>
</div>
@endsection
