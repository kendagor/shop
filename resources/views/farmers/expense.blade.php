@extends('layouts.template')

@section('content')
 


  <div class=" box box-success ">
  <br>
  <div class="login-logo">
    <a href=""><b>Avepo</b>Vet Services - Expense Section</a>
  </div>

   
<form method="POST" action="{{ url('storeexpense') }}">
@csrf
 <div class="box-body " >

<div class="form-group has-feedback"> 
<label for="role" class=" col-form-label text-md-right">{{ __('Expense Category') }}</label>
<select id="id" class="form-control" name="expense" value="{{ old('role') }}" required autofocus>
  <option >Motobike Expense</option>
  <option >Drugs </option>
  <option >Vet </option>
</select>                      
</div>



<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Date') }}</label>
 <input id="date" type="date"  class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="date" value="{{ old('date') }}" required autofocus>                      
</div>

<div class="form-group has-feedback">
<label for="id_no" class=" col-form-label text-md-right">{{ __('Vet Name ') }}</label>
 <select name="vet" class="form-control">
    @foreach($vet as $user)
<option value="{{$user->id}}">{{$user->name}}</option>
  @endforeach 
 </select>                        
</div>

<div class="form-group has-feedback">
<label for="cost" class=" col-form-label text-md-right">{{ __('Cost Incurred') }}</label>
 <input id="cost" type="text" placeholder="0.00" class="form-control{{ $errors->has('cost') ? ' is-invalid' : '' }}" name="cost" value="{{ old('cost') }}" required autofocus>
       @if ($errors->has('cost'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('cost') }}</strong>
      </span>
       @endif
                        
</div>
{{-- <div class="form-group has-feedback">
<label for="cost" class=" col-form-label text-md-right">{{ __('Reference (Optional)') }}</label>
 <input id="cost" type="text" placeholder="ReceiptNo , etc" class="form-control{{ $errors->has('cost') ? ' is-invalid' : '' }}" name="ref" value="{{ old('cost') }}" required autofocus>
       @if ($errors->has('ref'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('ref') }}</strong>
      </span>
       @endif
                        
</div> --}}
<div class="form-group has-feedback">
<label for="id_no" class=" col-form-label text-md-right">{{ __('Cost Breakdown') }}</label>
 <textarea rows="3" name="desc" class="form-control">   </textarea>                 
</div> 

 </div>
  <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          

      
    </form>

</div>
  <!-- /.login-box-body -->



<!-- jQuery 3 -->

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>



@endsection
