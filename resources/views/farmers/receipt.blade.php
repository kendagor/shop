@extends('layouts.template')
@section('content')
<div class="box-body ">
  
    
  <div id="myDiv" class="myDiv" style="margin:5% 40% 0% 33%;float:left; width:500px; box-shadow:0 0 3px #aaa; height:auto;color:#666;">
   
   <div style="width:100%; padding:10px; float:left; background:#00a65a; color:#fff; font-size:30px; text-align:center;">
   <div class="login-logo">
    <a href=""><b>Avepo</b>Vet Services Receipt</a>
  </div>
   </div>
    <div style="width:100%; padding:0px 0px;border-bottom:1px solid rgba(0,0,0,0.2);float:left;">
        <div style="width:30%; float:left;padding:10px;">
           
            <span style="font-size:14px;float:left; width:100%;">
               From <br>
               AvepoVetServices Ltd,
                info@aveposmartfarm.com
            </span>
       <span style="font-size:14px;float:left;width:100%;">
        To :<br>
        {{\App\Customer::all()->where('id',$d->customer)->first()->name}}
        
            </span>
      <span style="font-size:14px;float:left;width:100%;">
     
      </span>
        </div>
    
        <div style="width:40%; float:right;padding:">
            <span style="font-size:14px;float:right;  padding:10px; text-align:right;">
                <b>Date : </b>{{date('D-M-Y')}}
            </span>
      <span style="font-size:14px;float:right;  padding:10px; text-align:right;">
               <b>Receipt No# : </b>Avepo/00{{$d->id}}
            </span>
           
        </div>
    </div>
    


    
    
    <div style="width:100%; padding:0px; float:left;">
     
          <div style="width:100%;float:left;background:#efefef;">
            <span style="float:left; text-align:left;padding:10px;width:50%;color:#888;font-weight:600;">
            Decription
           </span>
         <span style="font-weight:600;float:left;padding:10px ;width:40%;color:#888;text-align:right;">
          Cost
        </span>
      </div>
    
      <div style="width:100%;float:left;">
            <span style="float:left; text-align:left;padding:10px;width:50%;color:#888;">
      This Is the cost of {{$d->service}} service
          
          {{--   <span style="font-size:10px; float:left; width:100%;">
                (The device model is   {{ \App\Device::all()->where('id',$device->device)->first()->model }}  and serial number is  {{ \App\Device::all()->where('id',$device->device)->first()->serialnumber }}  origin is {{$device->from}} )
            </span> --}}
           
        </span>
         <span style="font-weight:normal;float:left;padding:10px ;width:40%;color:#888;text-align:right;">
            {{$d->cost}}
        </span>
      </div>
      
   
         <div style="width:100%;float:left; background:#fff;">
           
         <span style="font-weight:600;float:right;padding:10px 0px;width:40%;color:#666;text-align:center;">
        <b>Total Cost:</b>   {{$d->cost}}/=
          <br>
          
          <b> Pending Balance : </b>{{$d->balance}}/=
        </span>
        <span style="font-weight:600;float:right;padding:10px 0px;width:40%;color:#666;text-align:center;">
         
        </span>
      </div>

    </div>
 </div> 

<div class="box-footer">

   <input type="button" value="Print Gatepass" class="btn btn-success" onclick="PrintDiv('myDiv')" />
</div>

</div>
    
    
    
    </section>

<script type="text/javascript">
   function PrintDiv(id) {
            var data=document.getElementById(id).innerHTML;
            var myWindow = window.open('', 'Eco-hub Gatepass', 'height=400,width=600');
            myWindow.document.write('<html><head><title>my div</title>');
            /*optional stylesheet*/ //myWindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
            myWindow.document.write('</head><body >');
            myWindow.document.write(data);
            myWindow.document.write('</body></html>');
            myWindow.document.close(); // necessary for IE >= 10

            myWindow.onload=function(){ // necessary if the div contain images

                myWindow.focus(); // necessary for IE >= 10
                myWindow.print();
                myWindow.close();
            };
        }
function myFunction() {
   $("myDiv").printElement();
}
</script>
    @endsection
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

</body>