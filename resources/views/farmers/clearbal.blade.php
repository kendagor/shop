@extends('layouts.template')

@section('content')
 


  <div class=" box box-success ">
  <br>
  <div class="login-logo">
    <a href=""><b>Avepo</b>Vet Services</a>
  </div>

   
<form method="POST" action="{{ url('clearbal/'.$view->id) }}">
@csrf
 <div class="box-body " >

<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Service') }}</label>
 <input id="date" type="text"  class="form-control" name="service"  value="{{$view->service}}" readonly="">
               
</div>
  

<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Date') }}</label>
 <input id="date" type="text"  class="form-control" name="date"  value="{{$view->date}}" readonly="">
               
</div>

<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Customer') }}</label>
 <input id="date" type="text"  class="form-control" name="customer"  value="{{\App\Customer::all()->where('id',$view->customer)->first()->name}}" readonly="">
               
</div>

<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Vet Officer') }}</label>
 <input id="date" type="text"  class="form-control" name="veti"  value="{{\App\User::all()->where('id',$view->vet)->first()->name}}" readonly="">
 <input type="hidden" name="vet" value="{{$view->vet}}">
               
</div>

<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Cost Of Service') }}</label>
 <input id="date" type="text"  class="form-control" name="cost"  value="{{$view->cost}}" readonly="">                     
</div>

<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Remaining Balance') }}</label>
 <input id="date" type="text"  class="form-control" name="balance"  value="{{$view->balance}}" readonly="">                     
</div>
@if(!$view->balance == 0 or !$view->balance < 0 )


<div class="form-group has-feedback">
<label for="paid" class=" col-form-label text-md-right">{{ __('Amount Paid*') }}</label>
 <input id="paid" type="text" placeholder="0.00" class="form-control{{ $errors->has('paid') ? ' is-invalid' : '' }}" name="paid" value="{{ old('paid') }}" required autofocus>
       @if ($errors->has('paid'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('paid') }}</strong>
      </span>
       @endif
                        
</div>

   @endif                

 </div>
 @if(!$view->balance == 0 or !$view->balance < 0 )
  <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

              @else
               <div class="box-footer">
               <a href="{{url('home')}}" class="label label-danger" >Customer Has No Outstanding Balance</a>
              </div>
  @endif     
</form>
</div>
@endsection
