 @extends('layouts.template')
@section('content')

 <div class=" box box-primary ">
   <br>
  <div class="login-logo">
    <a href=""><b>Avepo</b>Vet Services - farmers Expense </a>
  </div>
<div class="table-responsive">
  
   <table id="myTable1" class="table table-striped table-bordered " cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Vet Name</th>
                            <th>Expense Category</th>
                            <th>Amount (Ksh)</th>
                            <th>Description</th>
                          {{--   <th>Status</th>    --}}                                                     
                        </tr>
                    </thead>
                    <tbody>                    
                      @foreach($data as $d)  
                      <tr>
                      <td>EXPE#{{$d->id}}</td>
                      <td>{{$d->date}}</td>
                      <td>{{\App\User::all()->where('id',$d->vet)->first()->name}}</td>
                      <td>{{$d->expense}}</td>
                      <td>{{$d->cost}}</td>
                      <td>{{$d->desc}}</td>
                     {{--   <td  class="text text-success">{{$d->status}}</td>   --}}                 
                      </tr>
                      @endforeach                   
                    </tbody>
                 </table> 
                 <div><center>{{$data->render()}}</center></div>
  </div>
</div>
@endsection