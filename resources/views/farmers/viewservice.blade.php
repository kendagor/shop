@extends('layouts.template')
@section('content')

<div class=" box box-primary ">
   <br>
  <div class="login-logo">
    <a href=""><b>Avepo</b>Vet Services - farmers Service Track</a>
  </div>

   <table id="myTable1" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Customer</th>
                      {{--       <th>Phone No</th> --}}
                            <th>Service</th>
                            <th>Cost</th>
                            <th>Balance</th>
                            <th>Status</th>
                            <th rowspan="3">Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    
                      @foreach($data as $d)  <tr>
                      <td>#{{$d->id}}</td>
                      <td>{{$d->date}}</td>
                      <td>{{\App\Customer::all()->where('id',$d->customer)->first()->name}}</td>
                   {{--    <td>+254{{$d->phone}}</td> --}}
                      <td>{{$d->service}}</td>
                      <td>{{$d->cost}}</td>
                      <td>{{$d->balance}}</td>
                      <td>{{$d->status}}</td>
                      <td >
{{--           <a href="" href="{{ url('clearbal/'.$d->id) }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('on-form').submit();"> Clear Balance</a>
                       --}}
@if($d->balance < 0 or $d->balance == 0  )


                                    <form id="on-form" action="{{  url('print/'.$d->id) }}" method="post" >
                                      <button class="btn btn-success" value="Clear Balance"  >Receipt</button>
                                        @csrf
                                    </form>
@else

  <form id="on-form" action="{{  url('clearbal/'.$d->id) }}" method="get" > @csrf
  <button class="btn btn-warning" value="Clear Balance"  >Clear Balance</button>
   </form>
   
@endif
{{--  <form id="on-form" action="{{  url('drop-service/'.$d->id) }}" method="get" >
                                      <button class="btn btn-danger" value="Clear Balance"  >Delete</button>
     @csrf
   </form> --}}
                                  
                      </td>
                         </tr>
                      @endforeach
                   
                    </tbody>
          </table> 
  </div>
  
@endsection