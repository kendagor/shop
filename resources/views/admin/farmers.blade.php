@extends('layouts.template')

@section('content')
  <div class=" box box-primary ">
   <br>
  
<form method="post" action="{{ url('searchfarmer') }}">
@csrf
 <div class="box-body " >
<div class="col-md-4">
  <div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Full Name') }}</label>
<input id="date" type="text"  class="form-control" name="name"  placeholder="farmer name" >
               
</div>
</div>
<div class="col-md-4">
  <div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Enter Phone Number') }}</label>
 <input id="date" type="text"  class="form-control" name="phone" placeholder="e.g 711230230"   >
               
</div>
</div>
<div class="col-md-4">
  <div class="form-group has-feedback">
    <label for="name" class=" col-form-label text-md-right">{{ __('Action ') }}</label>
     <button type="submit" class="form-control btn btn-default">Search</button>
  </div>
</div>

</div>
  
</form>

</div>
<div class=" box box-primary ">
   <br>
  <div class="login-logo">
   <a href=""><b>DYMA</b>HARDWARE STORES CUSTOMERS</a>
  </div>

   <table id="myTable1" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                           {{--  <th>ID</th> --}}
                            <th>Name</th>
                            <th>Phone No</th>
                            <th>Location</th>
                       {{--      <th>Email Address</th> --}}
                            <th>Status</th>
                              <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($data as $d)

                    <tr>
                   {{--    <td>#{{$d->id}}</td> --}}
                      <td>{{$d->name}}</td>
                      <td>+254{{$d->phone}}</td>
                      <td>{{$d->location}}</td>
                    {{--   <td>{{$d->email}}</td> --}}
                      <td>Active</td>
                      <td>
                        <a href="{{url('farmer/edit/'.$d->id.'/'.csrf_token())}}" class="btn btn-primary"> Edit </a>
                        <a href="{{url('farmer/delete/'.$d->id.'/'.csrf_token())}}" class="btn btn-danger"> Delete </a>
                      </td>
</tr>
                      @endforeach
                    </tbody>
          </table> 
  </div>

@endsection
