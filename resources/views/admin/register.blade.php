@extends('layouts.template')

@section('content')
 


  <div class=" box box-success ">
  <br>
  <div class="login-logo">
    <a href=""><b>Dyma</b>Hardware Stores</a>
  </div>

   
<form method="POST" action="{{ url('regfarmer') }}">
@csrf
 <div class="box-body " >

<div class="form-group has-feedback"> 
<label for="role" class=" col-form-label text-md-right">{{ __('Register') }}</label>
<select id="relationship" oninput="test()" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" value="{{ old('role') }}" required autofocus>
  <option value="1">Customer</option>
  <option value="2">Cashier</option>
  <option value="0">Seller</option>
  
</select>

       @if ($errors->has('role'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('role') }}</strong>
      </span>
       @endif
                        
</div>

<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Full Name') }}</label>
 <input id="name" type="text" placeholder="Full Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
       @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('name') }}</strong>
      </span>
       @endif
                        
</div>




<div class="form-group has-feedback">
<label for="phone" class=" col-form-label text-md-right">{{ __('Phone Number') }}</label>
 <input id="phone" type="text" placeholder="Your Phone Number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required autofocus>
       @if ($errors->has('phone'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('phone') }}</strong>
      </span>
       @endif
                        
</div>

<div class="form-group has-feedback">
<label for="location" class=" col-form-label text-md-right">{{ __('Your Location') }}</label>
 <input id="location" type="text" placeholder="Your Location Name" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location" value="{{ old('location') }}" required autofocus>
       @if ($errors->has('location'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('location') }}</strong>
      </span>
       @endif
                        
</div>
        <div id="typew" style="display: none;">

          <div class="form-group has-feedback">
<label for="id_no" class=" col-form-label text-md-right">{{ __('ID/Passport No') }}</label>
 <input id="id_no" type="text" placeholder="ID & Passport Number" class="form-control{{ $errors->has('id_no') ? ' is-invalid' : '' }}" name="id_no" value="{{ old('id_no') }}"  autofocus>
       @if ($errors->has('id_no'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('id_no') }}</strong>
      </span>
       @endif
                        
</div>
            
     <div class="form-group has-feedback{{ __('E-Mail Address') }}">
<label for="name" class=" col-form-label text-md-right">{{ __('Email Address') }}</label>
        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  autofocus placeholder="Email Address">
   

           @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert"> 
          <strong>{{ $errors->first('email') }}</strong>
        </span>
           @endif
      
      </div>
 </div> 

 <div id="credit" style="display: none;">

 </div>

  <div id="invoice" style="display: none;">

 </div>


 </div>
  <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          

      
    </form>

</div>
  <!-- /.login-box-body -->

<!-- jQuery 3 -->
 <script type="text/javascript">
  function test(){
    var x =document.getElementById("relationship").value;
     var typew =document.getElementById("typew");
        if( x == 1){
           typew.style.display ="none";
     
        }
        else{
      typew.style.display ="block";
        }
    };
</script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>




@endsection
