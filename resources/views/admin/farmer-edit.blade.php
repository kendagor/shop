@extends('layouts.template')

@section('content')
 
  <div class=" box box-success ">
  <br>
  <div class="login-logo">
    <a href=""><b>Avepo</b>Vet Services Edit Customer Details</a>
  </div>

   
<form method="POST" action="{{ url('/farmer/edit/'.$data->id) }}">
@csrf
 <div class="box-body " >

<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Token') }}</label>
 <input id="name" type="text" placeholder="Full Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="token" value="{{$token}}" required autofocus readonly="">
       @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('name') }}</strong>
      </span>
       @endif
                        
</div>

<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Full Name') }}</label>
 <input id="name" type="text" placeholder="Full Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{$data->name}}" required autofocus>
       @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('name') }}</strong>
      </span>
       @endif
                        
</div>




<div class="form-group has-feedback">
<label for="phone" class=" col-form-label text-md-right">{{ __('Farmer Phone Number') }}</label>
 <input id="phone" type="text" placeholder="Your Phone Number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{$data->phone}}" required autofocus>
       @if ($errors->has('phone'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('phone') }}</strong>
      </span>
       @endif
                        
</div>

<div class="form-group has-feedback">
<label for="location" class=" col-form-label text-md-right">{{ __('Farmer  Location') }}</label>
 <input id="location" type="text" placeholder="Your Location Name" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location" value="{{$data->location}}" required autofocus>
       @if ($errors->has('location'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('location') }}</strong>
      </span>
       @endif
                        
</div>
 

 </div>
  <div class="box-footer">
                <button type="submit" class="btn btn-primary">EDIT {{$data->name}} DETAILS</button>
              </div>
          

      
    </form>

</div>


@endsection
