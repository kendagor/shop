@extends('layouts.template')

@section('content')
 
<div class=" box box-success ">
   <br>
  <div class="login-logo">
     <a href=""><b>DYMA</b>HARDWARE STORES EMPLOYEES</a>
  </div>

   <table id="myTable1" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID +</th>
                            <th>Name</th>
                            <th>Phone No</th>
                            <th>Location</th>
                            <th>Email Address</th>
                            <th>Status</th>                            
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($data as $d)
                    <tr>
                      <td>#AVEPO-E00{{$d->id}}</td>
                      <td>{{$d->name}}</td>
                      <td>+254{{$d->phone}}</td>
                      <td>{{$d->location}}</td>
                      <td>{{$d->email}}</td>
                      <td>Active</td>
</tr>
                      @endforeach
                    </tbody>
          </table> 
  </div>

@endsection
