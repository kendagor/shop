@extends('layouts.template')
@section('content')
 <div class=" box box-primary ">
   <br>
  
<form method="Post" action="{{ url('displayfarm') }}">
@csrf
 <div class="box-body " >
<div class="col-md-3">
	<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Farmer') }}</label>
<select class="form-control" name="farmer">
	 @foreach($farmer as $user)
<option value="{{$user->id}}">{{$user->name}}</option>
  @endforeach 
</select>
               
</div>
</div>
<div class="col-md-3">
	<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('From ') }}</label>
 <input id="date" type="date"  class="form-control" name="from"  >
               
</div>
</div>
<div class="col-md-3">
<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('To ') }}</label>
 <input id="date" type="date"  class="form-control" name="to"  >
               
</div>
</div>
<div class="col-md-3">
	<div class="form-group has-feedback">
		<label for="name" class=" col-form-label text-md-right">{{ __('Action ') }}</label>
	   <button type="submit" class="form-control btn btn-primary">Search</button>
	</div>
</div>

</div>
  
</form>

</div>

<div class=" box box-primary ">
   <br>
  <div class="login-logo">
    <a href=""><b>Avepo</b>Smartfarm - farmers Service Track</a>
  </div>

   <table id="myTable1" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Customer</th>
                            <th>Date</th>                           
                            <th>Service </th>
                            <th>Total Cost</th>
                            <th>Un Paid</th>
                            <th>Status</th>
                           
                            
                        </tr>
                    </thead>
                    <tbody>
                    
                      @foreach($data as $d)  <tr>
                      <td>#{{$d->id}}</td>
                       <td>{{\App\Customer::all()->where('id',$d->customer)->first()->name}}</td>
                      <td>{{$d->date}}</td>
                      
                      <td>{{$d->service}}</td>
                      <td class="text-green">Ksh{{$d->cost}}/=</td>
                      <td class="text-red">{{$d->balance}}</td>
                      <td>{{$d->status}}</td>
                   
                         </tr>
                      @endforeach
                   
                    </tbody>
          </table> 
         <div><center>{{$data->render()}}</center></div>
  </div>
@endsection