  @extends('layouts.template')

@section('content')
<?php
 $vet = \app\User::all()->where('role',0);
 $farmer = \App\Customer::all();


?>
 <div class=" box box-success ">
   <br>
  
<form method="Post" action="{{ url('search-trans') }}">
@csrf
 <div class="box-body " >
<div class="col-md-3">
  <div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Farmers') }}</label>
<select class="form-control" name="vet">
   <option value="" >Choose Farmer</option>
   @foreach($farmer as $user)
<option value="{{$user->id}}">{{$user->name}}</option>
  @endforeach 
</select>
               
</div>
</div>
<div class="col-md-3">
  <div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Vet') }}</label>
<select class="form-control" name="vet">
   <option value="" >Choose Vet</option>
   @foreach($vet as $user)
<option value="{{$user->id}}">{{$user->name}}</option>
  @endforeach 
</select>
               
</div>
</div>
<div class="col-md-2">
  <div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('From ') }}</label>
 <input id="date" type="date"  class="form-control" name="from"  >
               
</div>
</div>

<div class="col-md-2">
<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('To ') }}</label>
 <input id="date" type="date"  class="form-control" name="to"  >
               
</div>
</div>
<div class="col-md-2">
  <div class="form-group has-feedback">
    <label for="name" class=" col-form-label text-md-right">{{ __(' ') }}</label>
     <button type="submit" class="form-control btn btn-primary">Search</button>
  </div>
</div>

</div>
  
</form>

</div>

 <div class=" box box-primary ">
   <br>
  <div class="login-logo">
    <a href=""><b>Avepo</b>Smartfarm - farmers Transactions (Track)</a>
  </div>
<div class="table-responsive">
  
   <table id="myTable1" class="table table-striped table-bordered " cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Farmer Name</th>
                             <th>Served By</th>
                            <th>Date</th>
                           {{--  <th>Reference</th>
                            <th>Through</th> --}}
                            <th>Amount Paid</th>
                       
                           {{--  <th>Recorded By</th> --}}
                            <th>Status</th>
                                                        
                        </tr>
                    </thead>
                    <tbody>
                    
                      @foreach($data as $d)  <tr>
                      <td>#{{$d->id}}</td>
                      <td>{{\App\Customer::all()->where('id',$d->customer)->first()->name}}</td>
                       <td>{{\App\User::all()->where('id',$d->vet)->first()->name}}</td>
                      <td>{{$d->date}}</td>
                      <td>{{$d->amount}}</td>
                      
                   {{--    <td>{{\App\User::all()->where('id',$d->user)->first()->name}}</td> --}}
                       <td class="text text-success">{{$d->status}}</td>
                     
                   
                         </tr>
                      @endforeach
                   
                    </tbody>
          </table> 
  </div>
</div>

@endsection