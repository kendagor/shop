 @extends('layouts.template')

@section('content')
<?php
 $vet = \app\User::all()->where('role',0);

?>
 <div class=" box box-success ">
   <br>
  
<form method="Post" action="{{ url('search-expense') }}">
@csrf
 <div class="box-body " >
<div class="col-md-3">
  <div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Vet') }}</label>
<select class="form-control" name="vet">
   <option value="" >Choose Vet</option>
   @foreach($vet as $user)
<option value="{{$user->id}}">{{$user->name}}</option>
  @endforeach 
</select>
               
</div>
</div>
<div class="col-md-3">
  <div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Expe Type') }}</label>
<select id="id" class="form-control" name="expense" >
  <option value="" >Choose Expense</option>
  <option >Motobike Expense</option>
  <option >Drugs </option>
  <option >Vet </option>
</select> 
               
</div>
</div>
<div class="col-md-2">
  <div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('From ') }}</label>
 <input id="date" type="date"  class="form-control" name="from"  >
               
</div>
</div>

<div class="col-md-2">
<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('To ') }}</label>
 <input id="date" type="date"  class="form-control" name="to"  >
               
</div>
</div>
<div class="col-md-2">
  <div class="form-group has-feedback">
    <label for="name" class=" col-form-label text-md-right">{{ __(' ') }}</label>
     <button type="submit" class="form-control btn btn-primary">Search</button>
  </div>
</div>

</div>
  
</form>

</div>

 <div class=" box box-primary ">
   <br>
  <div class="login-logo">
    <a href=""><b>Avepo</b>Vet Services - Vet Expense Transactions (Track)</a>
  </div>
<div class="table-responsive">
  
   <table id="myTable1" class="table table-striped table-bordered " cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Expense_ID</th>
                               <th>Vet Name</th>
                            <th>Expense Date</th>
                         
                            <th>Type</th>
                                  <th>Amount Incurred</th>
                            
                            {{-- <th>Recorded By</th> --}}
                            <th>Status</th>
                                                        
                        </tr>
                    </thead>
                    <tbody>
                    
                      @foreach($data as $d)  <tr>
                      <td>#{{$d->id}}</td>
                       <td>{{\App\User::all()->where('id',$d->vet)->first()->name}}</td>
                      <td>{{$d->date}}</td>
                      <td>{{$d->type}}</td>
                      <td>{{$d->amount}}</td>
                     
                     {{--  <td>{{\App\User::all()->where('id',$d->user)->first()->name}}</td> --}}
                       <td class="text text-success">{{$d->status}}</td>
                     
                   
                         </tr>
                      @endforeach
                   
                    </tbody>
          </table> 
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#myTable1').DataTable();
} );
</script>

@endsection