@extends('layouts.log')

@section('content')

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
     <a href=""><b>DYMA</b>HARDWARE STORES</a>
  </div>
  <div class="login-box-body">
    <p class="login-box-msg">Signup to ir&c to start your session</p>
    <form method="POST" action="{{ route('register') }}">
                        @csrf


<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Full Name') }}</label>
 <input id="name" type="text" placeholder="Full Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
       @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('name') }}</strong>
      </span>
       @endif
                        
</div>

<div class="form-group has-feedback">
<label for="location" class=" col-form-label text-md-right">{{ __('Your Location') }}</label>
 <input id="location" type="text" placeholder="Your Location Name" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location" value="{{ old('location') }}" required autofocus>
       @if ($errors->has('location'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('location') }}</strong>
      </span>
       @endif
                        
</div>

<div class="form-group has-feedback">
<label for="phone" class=" col-form-label text-md-right">{{ __('Phone Number') }}</label>
 <input id="phone" type="text" placeholder="Your Phone Number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required autofocus>
       @if ($errors->has('phone'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('phone') }}</strong>
      </span>
       @endif
                        
</div>
                   
     <div class="form-group has-feedback{{ __('E-Mail Address') }}">
<label for="name" class=" col-form-label text-md-right">{{ __('Email Address') }}</label>
        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address">
     <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

           @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert"> 
          <strong>{{ $errors->first('email') }}</strong>
        </span>
           @endif
      
      </div>


      <div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Passsword') }}</label>
      <input type="password" name="password" required placeholder="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" >
         
 @if ($errors->has('password'))
     <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('password') }}</strong>
     </span>
@endif
       <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
    <label for="password-confirm" class=" col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                               <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>

       <div class="box-footer">
               <div class="col-xs-6">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I agree to the <a href="#">terms and Condition</a>
            </label>
          </div>
        </div>
         <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
 <a href="{{ route('login') }}" class="text-center">Login to the system</a>
    
              </div>

        </div>
    </form>

 
    <!-- /.social-auth-links -->

 
   

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>


</body>


@endsection
