@extends('layouts.template')

@section('content')

<section class="content-main">
	<div class="row">
		<div class="col-md-12">

	<div class="box box-info">
				<div class="box box-info">
					<div class="box-header">
						<h3 class="box-title">Search Item</h3>
					</div> <!-- end of box-header -->

					<div class="row">
						<form action="{{url('get-sale')}}" method="post"  >
								@csrf
						<div class=" col-md-4" style="padding-left:25px;padding-right:25px">
							
						  <div class="form-group has-feedback">
						  
						    <input  name="item" id="country_name" type="text" class="form-control " id="txtsearchitem" placeholder="Search item or item id here..." required="" />
						    <div id="countryList">
						    </div>
						   </div>
						</div>
						
						<div class=" col-md-3" style="padding-left:25px;padding-right:25px">
							
						  <div class="form-group has-feedback">
						  <select name="type" class="form-control">
						  	<option value="1">Retail</option>
						  	<option value="2">W sale</option>
						  </select>
						    
						   </div>
						</div>
						<div class=" col-md-3" style="padding-left:25px;padding-right:25px">
							
						  <div class="form-group has-feedback">
						  
						    <input  name="quantity"  type="text" class="form-control " id="txtsearchitem" placeholder="Quantity" required="" />
						   
						   </div>
						</div>

						<div class=" col-md-2" style="padding-left:25px;padding-right:25px">
							<div class="form-group has-feedback">
			{{-- 	<label for="id_no" class=" col-form-label text-md-right">{{ __('') }}</label>   --}}
                        <button  type="submit" class=" form-control btn btn-primary">Add Item </button>
                    </div>

						</div>
					</form>
					</div>				
			

				</div>

			</div><!-- end of div box info -->

			
	</div><!-- row -->
	
</div>
	<div class="row">
		<div class="col-md-8">
		  <div class="box box-solid box-info">
		
				<div class="box-header">
					<h1 class="box-title">Items For Purchase </h1>
				</div> <!-- end of box-header -->
				<div class="box-body">
					{{-- <center><h3>Dyma Holdings Limited,</h3>
						<h4>Po Box 278,Olenguruone,</h4>
					<h5>Tel: 0718392777</h5></center> --}}
</div>
							<div class="box-body">
						<div class="box-body table-responsive no-padding" style="max-width:900px;">
							<table id="table_search" class="table  table-bordered table-hover table-striped">
								<thead>
									<tr class="tableheader">
										<th style="width:40px">Item#</th>
										
										<th style="width:300px">Description</th>
										<th style="width:100px">Price</th>
										<th style="width:100px">Quantity</th>
										<th style="width:100px">Amount</th>
										<!-- <th style="width:120px">Stock</th> -->
									</tr>
								</thead>
								<tbody>
									@if(!empty($items))
									@foreach($items as $item)
									<tr>
										<td>{{$item->id}}</td>
										<td>{{$item->item}}</br>{{$item->desc}}</td>
										<td>Ksh{{$item->sprice}}</td>
										<td>{{$item->quantity}}</td>
										<td>Ksh{{$item->price}}</td>
									</tr>
									@endforeach
	
									@endif
								</tbody>
								
							</table>
						</div>				
					</div> 
					<div class="box-footer">
<span align="right">
	@if(!empty($items))
<h6>Sub-total: <b class="text-primary">Ksh {{$subprice}}</b></h6>
<h6>VAT(16%): <b class="text-danger">Ksh {{$vat}}</b></h6>
<h6>Total Amount: <b class="text-success">Ksh {{$price}}</b></h6>
@endif

</span>
					</div>
					<!-- end of box-body -->
				</div> <!-- end of box-body -->
			
			</div><!-- end of box box-solid bg-light-blue-gradiaent -->
	<div class="col-md-4">
		  <div class="box box-solid box-success">
		
				<div class="box-header">
					<h1 class="box-title"><center>Payment </center></h1>
				</div> <!-- end of box-header -->

				<div class="box-body">

								@if(!empty($items))
								<form method="post" action="{{url('make-sale')}}">
									@csrf
<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Cashier') }}</label>
 <input id="name" type="text" placeholder="Todays Seller" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="user" value="{{ Auth::user()->name }}" required autofocus readonly="">
       @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('name') }}</strong>
      </span>
       @endif
                        
</div>

<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Amount Due ') }}</label>
 <input id="name" type="text" placeholder="Todays Seller" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="amount" value="{{ $price }}" required autofocus readonly="">
       @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('name') }}</strong>
      </span>
       @endif
                        
</div>
<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Amount Paid') }}</label>
 <input id="name" type="text" placeholder="0.00" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="paid" value=""  autofocus >
       @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('name') }}</strong>
      </span>
       @endif
                        
</div>


<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Mode of Payment') }}</label>
 <select class="form-control" name="payment" id="paids" oninput="test()" >
 	<option value="">Select Mode Of Payment</option>
 	<option value="">Cash</option>
 	<option value="1">Mpesa</option>
 	<option value="2">Bank</option>
 	<option value="3">Credit</option>
 	<option value="4">Invoice</option>
 	<option value="5">Check</option>
 </select>
       @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('name') }}</strong>
      </span>
       @endif
                        
</div>
<div id="credits" style="display: none;">
<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Bank Name') }}</label>
 <input id="bank_name" type="text" placeholder="Full Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"  autofocus>
       @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('name') }}</strong>
      </span>
       @endif
                        
</div>

</div>

<div id="typew" style="display: none;">
<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Customer/Institution Name') }}</label>
 <input id="cname" type="text" placeholder="Full Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"  autofocus>
       @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('name') }}</strong>
      </span>
       @endif
                        
</div>




<div class="form-group has-feedback">
<label for="phone" class=" col-form-label text-md-right">{{ __('Phone Number') }}</label>
 <input id="cphone" type="text" placeholder="Your Phone Number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}"  autofocus>
       @if ($errors->has('phone'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('phone') }}</strong>
      </span>
       @endif
                        
</div>

<div class="form-group has-feedback">
<label for="location" class=" col-form-label text-md-right">{{ __(' Location') }}</label>
 <input id="clocation" type="text" placeholder="Your Location Name" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location" value="{{ old('location') }}"  autofocus>
       @if ($errors->has('location'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('location') }}</strong>
      </span>
       @endif
                        
</div>
</div>


<div>
	<button class="btn btn-success">Make Payment</button>
</div>

									
								</form>
								@endif
				</div>
		</div>
		</div><!-- end of col-md-8 -->

	</section>
<!--  -->
	<script>
$(document).ready(function(){

 $('#country_name').keyup(function(){ 
        var query = $(this).val();
        if(query != '')
        {
         var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"{{ route('autocomplete.fetch') }}",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
           $('#countryList').fadeIn();  
                    $('#countryList').html(data);
          }
         });
        }
    });

    $(document).on('click', 'li', function(){  
        $('#country_name').val($(this).text());  
        $('#countryList').fadeOut();  
    });  


});

 function test(){
    var x =document.getElementById("paids").value;

     var typew =document.getElementById("typew");
      var credit =document.getElementById("credits");
        if( x == 3 || x == 4 ){
           typew.style.display ="block";
     
        }
        else{
      typew.style.display ="none";
        }
         if( x == 2){
           credit.style.display ="block";
     
        }
        else{
      credit.style.display ="none";
        }
    };
</script>
 
</body>
</html>
@endsection
