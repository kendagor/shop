@extends('layouts.template')

@section('content')
<?php
$farmers = \App\Customer::all()->count();
$credit =\App\Farmer_service::all()->sum('balance');
$trans =\App\Transaction::all()->sum('amount');
$expenset =\App\Expensetransaction::all()->sum('amount');

?>

<section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-navy">
            <div class="inner">
              <h3>{{$farmers}}</h3>

              <p>Registered Customers</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="{{url('farmers')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{\App\Transaction::all()->sum('amount')}}<sup style="font-size: 20px">/=</sup></h3>

              <p>Cost of {{\App\Transaction::all()->count()}} Transaction</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{\App\User::all()->count()}}</h3>

              <p>User Registered</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$expenset}}/=</h3>

              <p>Total Expenses</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12 ">
          <!-- small box -->
          <div class="small-box ">
           <div id="container" class="box box-success" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
          
          
          </div>
       

     
      </div>
    

   
    </section>
    <section>
      
      <script type="text/javascript">
        Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Dyma Transactions'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Cash',
        colorByPoint: true,
        data: [{
            name: 'Expenses',
            y: {{ $expenset}},
            sliced: true,
            selected: true
        }, {
            name: 'Income',
            y: {{ $trans}}
        }, {
            name: 'Unpaid Balances ',
            y:{{ $credit}}
        }]
    }]
});
      </script>
    </section>
@endsection
