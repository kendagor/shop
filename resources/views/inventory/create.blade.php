@extends('layouts.template')

@section('content')
 


  <div class=" box box-success ">
  <br>
  <div class="login-logo">
    <a href=""><b>Dyma</b>Hardware Stock Entry Page</a>
  </div>

   
<form method="POST" action="{{ url('store-inventory') }}">
@csrf
 <div class="box-body " >


<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Entry done By') }}</label>
 <input id="name" type="text" placeholder="Stock Item Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="user" value="{{ Auth::user()->name }}" required autofocus readonly="">
       @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('name') }}</strong>
      </span>
       @endif
                        
</div>




<div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Item Name') }}</label>
 <input id="name" type="text" placeholder="Stock Item Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
       @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('name') }}</strong>
      </span>
       @endif
                        
</div>

<div class="form-group has-feedback">
<label for="desc" class=" col-form-label text-md-right">{{ __('Description') }}</label>
 <input id="desc" type="text" placeholder="Description of Stock" class="form-control{{ $errors->has('desc') ? ' is-invalid' : '' }}" name="desc" value="{{ old('desc') }}" required autofocus>
       @if ($errors->has('desc'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('desc') }}</strong>
      </span>
       @endif
                        
</div>
        
          <div class="form-group has-feedback">
<label for="bp" class=" col-form-label text-md-right">{{ __('Buying Price') }}</label>
 <input id="bp" type="text" placeholder="Enter The Buying Price" class="form-control{{ $errors->has('bp') ? ' is-invalid' : '' }}" name="bp" value="{{ old('bp') }}"  autofocus>
       @if ($errors->has('bp'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('bp') }}</strong>
      </span>
       @endif
                        
</div>

 <div class="form-group has-feedback">
<label for="sp" class=" col-form-label text-md-right">{{ __('Selling Price') }}</label>
 <input id="sp" type="text" placeholder="Enter The Selling Price" class="form-control{{ $errors->has('sp') ? ' is-invalid' : '' }}" name="sp" value="{{ old('sp') }}"  autofocus>
       @if ($errors->has('sp'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('sp') }}</strong>
      </span>
       @endif
                        
</div>

<div class="form-group has-feedback">
<label for="quantity" class=" col-form-label text-md-right">{{ __('Quantity Received') }}</label>
 <input id="quantity" type="text" placeholder="Quantity" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" name="quantity" value="{{ old('quantity') }}"  autofocus>
       @if ($errors->has('quantity'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('quantity') }}</strong>
      </span>
       @endif                        
</div>

<div class="form-group has-feedback">
<label for="unit" class=" col-form-label text-md-right">{{ __('Unit of Measure') }}</label>
 
<select  class="form-control{{ $errors->has('size') ? ' is-invalid' : '' }}" name="unit">
  <option value="">Select Units of Measure</option>
  <option>PCs</option>
  <option>Bags</option>
  <option>Pkts</option>
  <option>Kg</option>
  <option>MML</option>
  <option>g</option>
</select>
       @if ($errors->has('unit'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('unit') }}</strong>
      </span>
       @endif                        
</div>

<div class="form-group has-feedback">
<label for="size" class=" col-form-label text-md-right">{{ __('Size') }}</label>
 <input id="size" type="text" placeholder="Item Size" class="form-control{{ $errors->has('size') ? ' is-invalid' : '' }}" name="size" value="{{ old('size') }}"  autofocus>
       @if ($errors->has('size'))
      <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('size') }}</strong>
      </span>
       @endif                        
</div>

<div class="form-group has-feedback">
<label for="size" class=" col-form-label text-md-right">{{ __('Storage') }}</label>

<select  class="form-control{{ $errors->has('size') ? ' is-invalid' : '' }}" name="storage">
  <option value="">Select Storage</option>
  <option>OLE 1A</option>
  <option>OLE 1B</option>
  <option>OLE 1C</option>
  <option>Mzalendo 2A</option>
  <option>Mzalendo 2A</option>
  <option>Mzalendo 2A</option>
</select>
             
</div>
            
  
      <div id="typew" style="display: none;">

 </div> 


 </div>
  <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          

      
    </form>

</div>
  <!-- /.login-box-body -->

<!-- jQuery 3 -->
 <script type="text/javascript">
  function test(){
    var x =document.getElementById("relationship").value;
     var typew =document.getElementById("typew");
        if( x == 1){
           typew.style.display ="none";
     
        }
        else{
      typew.style.display ="block";
        }
    };
</script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>




@endsection
