@extends('layouts.template')

@section('content')
  <div class=" box box-primary ">
   <br>
  
<form method="get" action="{{ url('search-stock') }}">
@csrf
 <div class="box-body " >
<div class="col-md-4">
  <div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Item Name') }}</label>
<input id="date" type="text"  class="form-control" name="name"  placeholder="farmer name" >
               
</div>
</div>
<div class="col-md-4">
  <div class="form-group has-feedback">
<label for="name" class=" col-form-label text-md-right">{{ __('Enter Keywords') }}</label>
 <input id="date" type="text"  class="form-control" name="phone" placeholder="Nails 1 inch"   >
               
</div>
</div>
<div class="col-md-4">
  <div class="form-group has-feedback">
    <label for="name" class=" col-form-label text-md-right">{{ __('Action ') }}</label>
     <button type="submit" class="form-control btn btn-default">Search</button>
  </div>
</div>

</div>
  
</form>

</div>
<div class=" box box-primary ">
   <br>
  <div class="login-logo">
    <a href=""><b>Dyma</b>Shop Inventoty Services - Stock Records</a>
  </div>

   <table id="myTable1" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                           {{--  <th>Description</th> --}}
                            <th>Buying Price</th>
                            <th>Selling Price</th>
                            <th>Unit</th>
                            <th>Size</th>
                            <th>Stock Quantity</th>
                            <th>Location</th>
                            
                              <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($data as $d)

                    <tr>
                      <td>DYMA/00{{$d->id}}</td>
                      <td>{{$d->name}}</td>
                  {{--     <td>{{$d->description}}</td> --}}
                      <td>{{$d->bp}}</td>
                      <td>{{$d->sp}}</td>
                      <td>{{$d->size}}</td>
                      <td>{{$d->unit}}</td>
                      <td>{{$d->quantity}}</td>
                      <td>{{$d->store}}</td>
                     
                      <td>
                        <a href="{{url('inventory/edit/'.$d->id.'/'.csrf_token())}}" class="label label-primary"> Edit </a>
                        <a href="{{url('inventory/delete/'.$d->id.'/'.csrf_token())}}" class="label label-danger"> Delete </a>
                      </td>
</tr>
                      @endforeach
                    </tbody>
          </table> 
          <center>{{$data->render()}}</center>
  </div>

@endsection
