@extends('layouts.template')

@section('content')

<section class="content-main">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-solid box-danger">
				
				<div class="box-header">
					<h1 class="box-title">Dyma Holdings</h1>
				</div> <!-- end of box-header -->
				<div class="box-body">
					<h4>Olenguruone</h4>
					<h5>Telp : 0718392777</h5>
				</div> <!-- end of box-body -->
			</div><!-- end of box box-solid bg-light-blue-gradiaent -->
		</div>
	</div><!-- row -->
	<div class="row">
		<div class="col-md-9">
			<div class="box box-info">
				<div class="box box-info">
					<div class="box-header">
						<h3 class="box-title">Search Item</h3>
					</div> <!-- end of box-header -->

					<div class="row">
						<div class=" col-xs-12" style="padding-left:25px;padding-right:25px">
							<input type="text" class="form-control " id="txtsearchitem" placeholder="Search item or item id here...">
						</div>
					</div>				
					<div class="box-body">
						<div class="box-body table-responsive no-padding" style="max-width:900px;">
							<table id="table_search" class="table  table-bordered table-hover table-striped">
								<thead>
									<tr class="tableheader">
										<th style="width:40px">#</th>
										<th style="width:60px">Id</th>
										<th style="width:300px">Items</th>
										<th style="width:100px">Price</th>
										<th style="width:120px">Stock</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>				
					</div> <!-- end of box-body -->

				</div>

			</div><!-- end of div box info -->
		</div><!-- end of col-md-8 -->
		<div class="col-md-3">
			<div class="small-box bg-green">
				<div class="inner">
					<h3>30</h3>
					<p>Today Sales</p>
				</div>
				<div class="icon">
					<i class="ion ion-stats-bars"></i>
				</div>
				<a href="#" id="lapjuals" class="small-box-footer"> More Detail <i class="fa fa-arrow-circle-right"></i></a>
			</div><!-- end of small-box-green -->

			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>10</h3>
					<p>Out of stock items</p>
				</div>
				<div class="icon">
					<i class="ion ion-bag"></i>
				</div>
				<a href="application/mutasi/l_barang_abis.php" target="_blank" class="small-box-footer">Info detail <i class="fa fa-arrow-circle-right"></i></a>

			</div><!-- end of col md 4 -->
		</div><!-- end of row -->
	</section>
<!--  -->
	<script>
		$(document).on("keyup keydown","#txtsearchitem",function(){
			var searchitem = $("#txtsearchitem").val();
			value={
				term : searchitem,
			}
			$.ajax(
			{
				url : "../master/c_search_item.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					$("#table_search tbody").html(data.data)
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});

	</script>
</body>
</html>
@endsection
