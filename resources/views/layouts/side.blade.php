<aside class="main-sidebar" >
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
           <img src="{{asset('/images/'.Auth::user()->image)}}" onerror="this.src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAEfoi41KK3dJ9AQKIHNF5_7S9qGLfMbf9WbYMpTXJCm51aBLK'" class="img-circle" alt="User Image">
         =
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->username}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
    {{--   <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> --}}
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu tree" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
<li><a href="{{url('/pos')}}"><i class="fa fa-money"></i>Make Sale Service </a></li>

@if(Auth::user()->role == 1)
 

     <li class="treeview">
          <a href="#">
            <i class="fa fa-gears"></i> <span>Inventory</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu" style="display: none;">
               <li><a href="{{url('/create-inventory')}}"><i class="fa fa-circle-o"></i>Add Inventory </a></li>
                <li><a href="{{url('/inventory')}}"><i class="fa fa-circle-o"></i> View Inventory </a></li>
          </ul>
      
        </li>
{{-- risk families --}}
       <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Registration</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu" style="display: none;">
               <li><a href="{{url('/registerfarmer')}}"><i class="fa fa-circle-o"></i>Registration </a></li>
                <li><a href="{{url('/farmers')}}"><i class="fa fa-circle-o"></i> Customers </a></li>
                <li><a href="{{url('/vets')}}"><i class="fa fa-circle-o"></i> Employees </a></li>
          </ul>
      
        </li>

      <li class="treeview">
          <a href="#">
            <i class="fa fa-asterisk"></i> <span>Services</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu" style="display: none;">
               <li><a href="{{url('/newservice')}}"><i class="fa fa-circle-o"></i>Record New Service </a></li>
                <li><a href="{{url('/viewservice')}}"><i class="fa fa-circle-o"></i> Farmers Service records </a></li>
          </ul>
      
        </li>

           <li class="treeview">
          <a href="#">
            <i class="fa fa-money"></i> <span>Expenses</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu" style="display: none;">
               <li><a href="{{url('/newexpense')}}"><i class="fa fa-circle-o"></i>Record New Expense </a></li>
                <li><a href="{{url('/view-expe')}}"><i class="fa fa-circle-o"></i> View Expenses </a></li>
          </ul>
      
        </li>

           <li class="treeview">
          <a href="#">
            <i class="fa fa-bar-chart"></i> <span>Reports</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu" style="display: none;">
               <li><a href="{{url('/expense-trans')}}"><i class="fa fa-circle-o"></i>Expense Transactions </a></li>
                <li><a href="{{url('/income-trans')}}"><i class="fa fa-circle-o"></i> Income Transactions </a></li>
                  <li><a href="{{url('/farmers-report')}}"><i class="fa fa-circle-o"></i> Farmers Reports </a></li>
          </ul>
      
        </li>


@endif
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
