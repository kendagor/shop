<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
   protected $fillable = [
        'item', 'desc', 'quantity','sprice','price','user',
    ];

}
