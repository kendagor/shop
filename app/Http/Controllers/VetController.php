<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Customer;
use App\Farmer_service;
use App\Transaction;
use App\Expense;
use App\Expensetransaction;
use Auth;
use PdfReport;
class VetController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    $data = Customer::all();
   return view('admin.register',compact('data'));
    }

    public function farmers()
    {
    $data = Customer::all();
    return view('admin.farmers',compact('data'));
    }
     public function vets()
    {
    $data = User::all()->where('role',0);
    return view('admin.vet',compact('data'));
    }

//expense
     public function expense()
    {
    $data = Expense::all();
    $vet = User::all();
    return view('farmers.expense',compact('data','vet','expense'));
    }
//expense
     public function viewexpense()
    {
     $data = Expense::all();
    $vet = User::all();
    return view('farmers.expense',compact('data','vet','expense'));
    }
   //view Expense
       public function serviceexpe()
    {
     $data = Expense::paginate(15);
    $vet = User::all();
    return view('farmers.viewtransaction',compact('data','vet','expense'));
    } 

public function dropexpense($id)
    {
     $data = Expense::findorfail($id);
    $data->delete();
    return back()->with('status','Successfull');
    } 
public function dropservice($id)
    {
     $data = Farmer_service::findorfail($id);
    $data->delete();
    return back()->with('status','Successfull');
    } 

     public function pays($id)
    {
    $view = Farmer_service::all()->where('id',$id)->first();
    $data = Farmer_service::all();
    $vet = User::all();
   return view('farmers.clearbal',compact('view','vet','data'));
    }
      public function service()
    {
    $data = Farmer_service::all();
    $customer = Customer::all();
    $vet = User::all();
   return view('farmers.service',compact('data','vet','customer'));
    }

  public function viewservice()
    {
    $data = Farmer_service::all();
    $customer = Customer::all();
    $vet = User::all();
   return view('farmers.viewservice',compact('data','vet','customer'));
    }

    public function reportetrans()
    {
        $data = Expensetransaction::all();
       return view('reports.expenses',compact('data'));
    }

     public function print($id)
    {
        $d = Farmer_service::all()->where('id',$id)->first();
       return view('farmers.receipt',compact('d'));
    }
       public function reportitrans()
    {
        $data = Transaction::all();
       return view('reports.transactions',compact('data'));
    }

   public function farmrepo()
    {
        $data = Farmer_service::paginate(20);
        $farmer = Customer::all();
       return view('reports.farmers',compact('data','farmer'));
    }

//store exe
  public function storeexpense(Request $request)
    {
        
       $expense = new Expense();
       $expense->date = $request->input('date');
       $expense->expense = $request->input('expense');
       // $expense->type = $request->input('type');
       $expense->vet = $request->input('vet');
       $expense->cost = $request->input('cost');
       $expense->desc = $request->input('desc');
       // $expense->ref = $request->input('ref');
       $expense->user = Auth::id();
       $expense->status = 'Captured';  

       $Etransaction = new Expensetransaction();
       $Etransaction->date = \Carbon\Carbon::now();
       $Etransaction->type = $request->input('expense');
       $Etransaction->amount = $request->input('cost');
       $Etransaction->vet = $request->input('vet');
       $Etransaction->user = Auth::id();
       $Etransaction->status = 'Complete';
      
            
       $expense->save();
       $Etransaction->save();   


       return back()->with('status','Successfully registered '.$request->input('vet').' Expense');
    }
    //
    public function store(Request $request)
    {
        
    if ($request->input('role') == 1) {
       $farmer = new Customer();
       $farmer->name = $request->input('name');
       $farmer->phone = $request->input('phone');
       // $farmer->id_no = $request->input('id_no');
       // $farmer->email = $request->input('email');
       $farmer->location = $request->input('location');
       $farmer->role =  'Farmer';
       }
       else
       {
       $farmer = new User();
       $farmer->name = $request->input('name');
       $farmer->phone = $request->input('phone');
       $farmer->id_no = $request->input('id_no');
       $farmer->email = $request->input('email');
       $farmer->location = $request->input('location');
       $farmer->password = bcrypt(12345678);
       $farmer->role =  'Vet';
       }
      
       $farmer->save();

       return back()->with('status','Successfully registered '.$request->input('name').'');
    }
    public function servicestore(Request $request)
    {
        
        $balance = (($request->input('cost'))-($request->input('paid')));
       $Sfarmer = new Farmer_service();
       $Sfarmer->date = $request->input('date');
       $Sfarmer->customer = $request->input('customer');
       $Sfarmer->vet = $request->input('vet');
       $Sfarmer->service = $request->input('service');
       $Sfarmer->desc = $request->input('desc');
       $Sfarmer->cost = $request->input('cost');
       $Sfarmer->paid = $request->input('paid');
       $Sfarmer->balance = $balance;
       $Sfarmer->cost = $request->input('cost');
       // $Sfarmer->means = $request->input('means');
       // $Sfarmer->ref = $request->input('ref');
       if ($balance == 0) {
       $Sfarmer->status = 'Complete';
       }
       else
       {
        $Sfarmer->status = 'Incomplete';
       }
       $transaction = new Transaction();
       $transaction->date = \Carbon\Carbon::now();
       // $transaction->means = $request->input('means');
       // $transaction->ref = $request->input('ref');
       $transaction->amount = $request->input('paid');
       $transaction->customer = $request->input('customer');
       $transaction->user = Auth::id();
       $transaction->vet = $request->input('vet');
       $transaction->status = 'Complete';
       $Sfarmer->save();
       $transaction->save();

       return back()->with('status','Successfully  Captured '.$request->input('name').' Transaction');
    }

     public function clearpay(Request $request,$id)
    {
        
       $balance = Farmer_service::all()->where('id',$id)->first()->balance;
       $customer = Farmer_service::all()->where('id',$id)->first()->customer;
       $paid = Farmer_service::all()->where('id',$id)->first()->paid;
       $newbal = (($balance) - ($request->input('paid')));
       $farmer = Farmer_service::findorfail($id);
       $farmer->paid = (($paid) + ($request->input('paid')));
       $farmer->balance = $newbal;
      if ($newbal == 0) {
       $farmer->status = 'Complete';
       }
       else
       {
        $farmer->status = 'Incomplete';
       }
       $transaction = new Transaction();
       $transaction->date = \Carbon\Carbon::now();
       $transaction->vet = $request->input('vet');
       $transaction->amount = $request->input('paid');
       $transaction->customer = $customer;
       $transaction->user = Auth::id();
       $transaction->status = 'Complete';
       $farmer->save();
       $transaction->save();
       return back()->with('status','Successfully  Captured '.$customer.' Balance Payment');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    public function farmeredit($id,$token)
    {
        $data = Customer::all()->where('id',$id)->first();

        return view('admin.farmer-edit',compact('data','token'));
    }
     public function afarmeredit(Request $request,$id)
    {
         $farmer =Customer::findorfail($id);
         $farmer->name = $request->input('name');
        $farmer->phone = $request->input('phone');
        $farmer->location = $request->input('location');
       $farmer->role =  'Farmer';
       $farmer->save();
 return redirect('farmers')->with('status','Successfully Edited '.$request->input('name').'');
      
    }
      public function farmerdelete($id,$token)
    {
       $farmer =Customer::findorfail($id);
       $farmer->delete();
        return back()->with('error','Successfully Deleted');
    }


    //reports

    public function displayReport(Request $request)
{
    $fromDate = $request->input('from');
    $toDate = $request->input('to');
    $customer = $request->input('farmer');
    // $sortBy = $request->input('sort_by');
// Farmer_service::all()
//   ->where('customer',$customer)
//   ->where('date', '>=', $fromDate)
//   ->where('date', '<=', $toDate)->chunk(20);
  $data = Farmer_service::where(function($query) use ($request) {
     if ($request->input('farmer')) {
         $query->where('customer', '=', $request->input('farmer'));
     }

     if ($request->input('from')) {
         $query->where('date', '>=', $request->input('from'));
     }
      if ($request->input('to')) {
         $query->where('date', '<=', $request->input('to'));
     }
})->paginate(10);
 $farmer = Customer::all();
       return view('reports.farmers',compact('data','farmer'));
}

 public function searchfarmer(Request $request)
{
  $data = Customer::where(function($query) use ($request) {
     if ($request->input('name')) {
         $query->where('name', '=', $request->input('name'));
     }

     if ($request->input('phone')) {
         $query->where('phone', '=', $request->input('phone'));
     }
    
})->get();
   
    return view('admin.farmers',compact('data'));
}
 public function searchexpense(Request $request)
{
  $data = Expensetransaction::where(function($query) use ($request) {
     if ($request->input('vet')) {
         $query->where('vet', '=', $request->input('vet'));
     }

     if ($request->input('expense')) {
         $query->where('type', '=', $request->input('expense'));
     }
      if ($request->input('from')) {
         $query->where('date', '>=', $request->input('from'));
     }
      if ($request->input('to')) {
         $query->where('date', '<=', $request->input('to'));
     }
    
})->get();
   
    return view('reports.expenses',compact('data'));
}
public function searchtrans(Request $request)
{
  $data = Transaction::where(function($query) use ($request) {
     if ($request->input('farmer')) {
         $query->where('customer', '=', $request->input('farmer'));
     }

     if ($request->input('vet')) {
         $query->where('vet', '=', $request->input('vet'));
     }
      if ($request->input('from')) {
         $query->where('date', '>=', $request->input('from'));
     }
      if ($request->input('to')) {
         $query->where('date', '<=', $request->input('to'));
     }
    
})->get();
   
    return view('reports.transactions',compact('data'));
}
}
