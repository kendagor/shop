<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Inventory;
use App\Customer;
use DB;
class InventoryController extends Controller
{
    
    public function index()
    {
       $data = Inventory::orderBy('name','Asc')->paginate(10);

       return view('inventory.index',compact('data'));
    }

  function fetch(Request $request)
    {
     if($request->get('query'))
     {
      $query = $request->get('query');
      $data = DB::table('inventories')
        ->where('name', 'LIKE', "%{$query}%")
        ->get();
      $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
      foreach($data as $row)
      {
       $output .= '
       <li><a href="#">'.$row->name.'</a></li>
       ';
      }
      $output .= '</ul>';
      echo $output;
     }
    }
    public function create()
    {
      return view('inventory.create');
    }

  
    public function store(Request $request)
    {
       $store =  new Inventory();
       $store->name = $request->input('name');
       $store->description = $request->input('desc');
       $store->sp = $request->input('sp');
       $store->bp = $request->input('bp');
       $store->size = $request->input('size');
       $store->discount = $request->input('discount');
       $store->unit = $request->input('unit');
       $store->store = $request->input('store');
       $store->section = $request->input('section');
       $store->keywords = $request->input('keywords');
       $store->quantity = $request->input('quantity');
       $store->tax = $request->input('tax');
       // $store->status = 'Active';
       $store->save();
 return back()->with('status','Successfully registered '.$request->input('name').'');

    }

   
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$token)
    {
   $data = Inventory::all()->where('id',$id)->first();
   return view('inventory.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $store =   Inventory::findorfail($id);
       $store->name = $request->input('name');
       $store->description = $request->input('desc');
       $store->sp = $request->input('sp');
       $store->bp = $request->input('bp');
       $store->size = $request->input('size');
       $store->discount = $request->input('discount');
       $store->unit = $request->input('unit');
       $store->store = $request->input('store');
       $store->section = $request->input('section');
       $store->keywords = $request->input('keywords');
       $store->quantity = $request->input('quantity');
       $store->tax = $request->input('tax');
       // $store->status = 'Active';
       $store->save();
 return back()->with('status','Successfully registered '.$request->input('name').'');
    }

  public function searchstock(Request $request)
{
  $data = Inventory::where(function($query) use ($request) {
     if ($request->input('name')) {
         $query->where('name', '=', $request->input('name'));
     }

     if ($request->input('keywords')) {
         $query->where('keyboard', '=', $request->input('keyboard'));
     }
      
    
})->paginate(10);
   return view('inventory.index',compact('data'));
   
   
}
    public function destroy($id)
    {
        //
    }
}
