<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//dashboard
Route::get('/dashboard', 'HomeController@index');
Route::get('/pos', 'SaleController@index');
Route::post('/make-sale', 'SaleController@sales');

//vet
Route::get('/registerfarmer', 'VetController@index');
Route::get('/farmers', 'VetController@farmers');
Route::get('/vets', 'VetController@vets');
Route::post('/regfarmer', 'VetController@store');

Route::get('/farmer/edit/{id}/{token}', 'VetController@farmeredit');
Route::get('/farmer/delete/{id}/{token}', 'VetController@farmerdelete');
Route::post('/farmer/edit/{id}', 'VetController@afarmeredit');
Route::post('/farmer/delete', 'VetController@afarmerdelete');

Route::post('/searchfarmer', 'VetController@searchfarmer');
//sales
Route::get('/newservice', 'VetController@service');
Route::get('/viewservice', 'VetController@viewservice');
Route::get('/view-expe', 'VetController@serviceexpe');
Route::post('/payservice', 'VetController@servicestore');
Route::get('/clearbal/{id}', 'VetController@pays');
Route::post('/clearbal/{id}', 'VetController@clearpay');
Route::post('/print/{id}', 'VetController@print');

//expense
Route::get('/newexpense', 'VetController@expense');
Route::post('/storeexpense', 'VetController@storeexpense');

Route::get('/expense-trans', 'VetController@reportetrans');
Route::get('/income-trans', 'VetController@reportitrans');
Route::get('/farmers-report', 'VetController@farmrepo');
Route::post('/displayfarm', 'VetController@displayReport');
Route::post('/search-expense', 'VetController@searchexpense');
Route::post('/search-trans', 'VetController@searchtrans');

//
Route::post('/drop-expense/{id}', 'VetController@dropexpense');
Route::post('/drop-transaction/{id}', 'VetController@dropservice');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//inventory
Route::get('/inventory', 'InventoryController@index')->name('home');
Route::get('/create-inventory', 'InventoryController@create');
Route::post('/store-inventory', 'InventoryController@store');
Route::get('/inventory/delete/{id}', 'InventoryController@destroy');
Route::get('/search-stock', 'InventoryController@searchstock');
Route::post('update/inventory/{id}', 'InventoryController@update');
Route::get('/inventory/edit/{id}/{token}', 'InventoryController@edit');


Route::post('get-sale', 'SaleController@get');
Route::post('/autocomplete/fetch', 'InventoryController@fetch')->name('autocomplete.fetch');

