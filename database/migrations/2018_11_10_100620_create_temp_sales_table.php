<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->integer('uniqid');
            $table->date('input_date');
            $table->integer('id_item');
            $table->integer('item_name');
            $table->integer('qty');
            $table->string('unit');
            $table->integer('price');
            $table->integer('discprc');
            $table->integer('discrp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_sales');
    }
}
