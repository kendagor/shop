<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item');
            $table->string('desc')->nullable();
            $table->string('quantity')->default(1);;
            $table->string('sprice');
            $table->string('price');
            $table->string('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_sales');
    }
}
