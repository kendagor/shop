<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTSaleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_sale_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('autoid');
            $table->string('sale_id');
            $table->string('id_item');
            $table->string('item_name');
            $table->integer('qty')->nullable();
            $table->string('unit');
            $table->integer('price')->nullable();
            $table->integer('disc_prc')->nullable();
            $table->integer('disc_rp')->nullable();
          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_sale_details');
    }
}
