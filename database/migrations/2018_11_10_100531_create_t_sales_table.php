<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sale_id')->nullable();
            $table->integer('id_user')->nullable();
            $table->date('sale_date')->nullable();
            $table->string('input_date')->nullable();
            $table->integer('paid')->nullable();
            $table->integer('disc_prcn')->nullable();
            $table->integer('disc_rp')->nullable();
            $table->integer('sts')->nullable();
            $table->text('note')->nullable();
            
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_sales');
    }
}
